﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WorkTrackerFunctionsApp.Context;

[assembly: FunctionsStartup(typeof(WorkTrackerFunctionsApp.Startup))]

namespace WorkTrackerFunctionsApp
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            string connectionString = Environment.GetEnvironmentVariable("WorkTimeTrackerDb");
            builder.Services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlServer("Server=tcp:bkrasicki-cdv.database.windows.net,1433;Initial Catalog=WorkTimeTrackerDb;Persist Security Info=False;User ID=brasicki;Password=Bkr4sicki!;" +
                "MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
        }
    }
}
