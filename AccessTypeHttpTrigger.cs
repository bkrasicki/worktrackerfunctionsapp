using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WorkTrackerFunctionsApp.Context;
using System.Linq;
using WorkTrackerFunctionsApp.Models;
using System.Net.Http;
using System.Net;
using System.Text;

namespace WorkTrackerFunctionsApp
{
    public class AccessTypeHttpTrigger
    {
        private readonly ApplicationDbContext _context;

        public AccessTypeHttpTrigger(ApplicationDbContext context)
        {
            _context = context;
        }

        [FunctionName("GetAccessType")]
        public HttpResponseMessage Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "worker/{cardNumber}")] HttpRequest req,
            ILogger log, string cardNumber)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            try
            {
                var worker = _context.Workers.SingleOrDefault(w => w.CardNumber == cardNumber);
                if (worker == null)
                {
                    var workerState = new { state = (int)WorkerState.UnassignedCard };
                    var jsonToReturn = JsonConvert.SerializeObject(workerState);

                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
                    };
                }
                else if (!worker.HasAccess)
                {
                    var workerState = new { state = (int)WorkerState.NoAccess };
                    var jsonToReturn = JsonConvert.SerializeObject(workerState);

                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
                    };
                }
                else
                {
                    var workerState = new { state = (int)WorkerState.EntryOk };
                    var jsonToReturn = JsonConvert.SerializeObject(workerState);

                    return new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StringContent(jsonToReturn, Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                log.LogError(ex, ex.Message);
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
