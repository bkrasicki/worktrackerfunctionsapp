using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WorkTrackerFunctionsApp.Context;
using WorkTrackerFunctionsApp.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace WorkTrackerFunctionsApp
{
    public class EntryHistoryHttpTrigger
    {

        private readonly ApplicationDbContext _context;

        public EntryHistoryHttpTrigger(ApplicationDbContext context)
        {
            _context = context;
        }

        [FunctionName("AddEntryHistoryLog")]
        public IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "worker/addentryhistory")] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            var content = new StreamReader(req.Body).ReadToEnd();
            var worker = JsonConvert.DeserializeObject<Worker>(content);
            
            var workerInDb = _context.Workers.SingleOrDefault(w => w.CardNumber == worker.CardNumber);
            if (workerInDb == null || !workerInDb.HasAccess)
                return new NotFoundResult();

            var lastWorkerLog = _context.EntryHistory
                .Include(w => w.Worker)
                .OrderByDescending(wh => wh.Date)
                .FirstOrDefault(wh => wh.Worker.CardNumber == worker.CardNumber);

            if (lastWorkerLog == null || lastWorkerLog.Type == (int)EntryState.Out)
            {
                var entryHistory = new EntryHistory
                {
                    Type = (int)EntryState.In,
                    Date = DateTime.Now,
                    Worker = workerInDb
            };

                _context.EntryHistory.Add(entryHistory);
                _context.SaveChanges();
                return new OkObjectResult(entryHistory);
            }
            else
            {
                var format = @"dd\:hh\:mm\:ss";
                var entryHistory = new EntryHistory
                {
                    Type = (int)EntryState.Out,
                    Date = DateTime.Now,
                    Worker = lastWorkerLog.Worker,
                    WorkingTime = (DateTime.Now - lastWorkerLog.Date).ToString(format)
                };
                _context.EntryHistory.Add(entryHistory);
                _context.SaveChanges();
                return new OkObjectResult(entryHistory);
            }
        }
    }
}
