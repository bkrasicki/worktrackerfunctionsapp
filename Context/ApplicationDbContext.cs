﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WorkTrackerFunctionsApp.Models;

namespace WorkTrackerFunctionsApp.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base (options)
        {

        }

        public DbSet<Worker> Workers { get; set; }
        public DbSet<EntryHistory> EntryHistory { get; set; }
    }
}
