﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTrackerFunctionsApp.Models
{
    public enum WorkerState
    {
        UnassignedCard = 0,
        NoAccess = 1,
        EntryOk = 2
    }
}
