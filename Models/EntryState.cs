﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTrackerFunctionsApp.Models
{
    public enum EntryState
    {
        In = 0,
        Out = 1
    }
}
