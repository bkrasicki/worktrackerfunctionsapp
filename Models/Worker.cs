﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace WorkTrackerFunctionsApp.Models
{
    public class Worker
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public bool HasAccess { get; set; }

        public string CardNumber { get; set; }
    }
}
